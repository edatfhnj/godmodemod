﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using UnityModManagerNet;
using Harmony12;



namespace GodModeMod
{
    public class Setting : UnityModManager.ModSettings
    {
        public override void Save(UnityModManager.ModEntry modEntry)
        {
            base.Save(modEntry);
        }
    }
    public class MainMod
    {
        /// <summary>
        /// 确定MOD是开启的
        /// </summary>
        public static bool enable;
        /// <summary>
        /// 玩家的设置文件
        /// </summary>
        public static Setting settings;
        /// <summary>
        /// 游戏中输出信息
        /// </summary>
        public static UnityModManager.ModEntry.ModLogger logger;

        public static bool Load(UnityModManager.ModEntry modEntry)
        {
            //读取配置
            settings = Setting.Load<Setting>(modEntry);
            //存储日志
            logger = modEntry.Logger;

            //打开mod配置时
            modEntry.OnGUI = OnGUI;
            //保存mod配置时
            modEntry.OnSaveGUI = OnSaveGUI;
            //开关mod时
            modEntry.OnToggle = OnToggle;

            //更改应用到游戏
            var harmony = HarmonyInstance.Create(modEntry.Info.Id);
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            return true;
        }

        private static bool OnToggle(UnityModManager.ModEntry arg1, bool arg2)
        {
            enable = arg2;
            return true;
        }

        private static void OnSaveGUI(UnityModManager.ModEntry obj)
        {
            settings.Save(obj);
        }

        public static float MaxHp = 0f, total_Maxhp = 0f, Hp = 0f;
        public static int WeaponATK = 0;
        public static bool setValue = false;

        private static void OnGUI(UnityModManager.ModEntry obj)
        {
            //GUILayout.BeginHorizontal();
            //GUILayout.Label("God mode");
            //GUILayout.Space(30);
            //if (GUILayout.Button("set"))
            //{
            //    SetVar();
            //}
        }


        public static void SetVar()
        {
            if (!enable && setValue)
            {
                StaticMng.MaxHp= MaxHp;
                PlayerStatus.Instance.total_Maxhp= total_Maxhp;
                PlayerStatus.Instance.Hp=Hp;
                PlayerStatus.Instance.WeaponATK= WeaponATK;
                setValue = false;
                logger.Log(" unsetValue");
                return;
            }
            if (float.IsNaN(PlayerStatus.Instance.Hp))
            {
                logger.Log(" hp = null ");
                return;
            }
            MaxHp = StaticMng.MaxHp;
            total_Maxhp = PlayerStatus.Instance.total_Maxhp;
            Hp = PlayerStatus.Instance.Hp;
            WeaponATK = PlayerStatus.Instance.WeaponATK;
            setValue = true;
            StaticMng.MaxHp = 1000f;
            PlayerStatus.Instance.total_Maxhp = 1000f;
            PlayerStatus.Instance.Hp = 1000f;
            PlayerStatus.Instance.WeaponATK = 1000;
            logger.Log(" setValue");

            //var mesh = PlayerStatus.Instance.objplayer.GetComponent<MeshFilter>().mesh.name;
            //logger.Log("after change hp : " + PlayerStatus.Instance.Hp.ToString());
            //logger.Log("after change atk : " + PlayerStatus.Instance._ATK.ToString());
            //logger.Log("mesh name  : " + mesh);
        }
    }

}
